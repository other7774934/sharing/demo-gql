package com.example.demogql.controller;

import com.example.demogql.domain.Company;
import com.example.demogql.domain.Department;
import com.example.demogql.domain.Staff;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GraphqlController {

    final static List<Staff> staffs =
            List.of(
                    new Staff(1, "jack", 300.5f),
                    new Staff(2, "bora", 250.16f)
            ).stream().collect(Collectors.toList());

    final static List<Department> departments =
            List.of(
                    new Department(1, "IT", 12),
                    new Department(2, "Sale", 1)
            ).stream().collect(Collectors.toList());


    @QueryMapping
    public Staff getStaff(@Argument("id") int id) {
        return staffs.stream().filter(m -> m.getId() == id).findFirst().orElse(null);
    }

    @QueryMapping
    public List<Staff> getStaffs() {
        return staffs;
    }

    @QueryMapping
    public Company getCompany(
            @Argument("isStaff") boolean isStaff,
            @Argument("id") int id
    ) {
        if (isStaff) {
            return staffs.stream().filter(m -> m.getId() == id).findFirst().orElse(null);
        } else {
            return departments.stream().filter(m -> m.getId() == id).findFirst().orElse(null);
        }
    }

    @QueryMapping
    public List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();
        companies.addAll(staffs);
        companies.addAll(departments);
        return companies;
    }

    @MutationMapping
    public Staff createStaff (
            @Argument("name") String name,
            @Argument("salary") float salary
    ) {
        Staff staff = Staff.builder()
                .id(staffs.size() + 1)
                .name(name)
                .salary(salary).build();

        staffs.add(staff);
        return staff;
    }

}
