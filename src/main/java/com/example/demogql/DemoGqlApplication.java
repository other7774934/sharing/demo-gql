package com.example.demogql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGqlApplication.class, args);
	}

}
